
//const Chat = require('./menu.model');
const Sequelize = require('../../sequelize');
const JWT = require('./../../jwt');

const socketIO = require('./../../socket-io')

exports.getMenu = function(request, response){
    response.status(200) //OK
    response.json(
        {
            menu: [
                {
                    name: 'Usuarios',
                    icon: 'fa-users',
                    badge: undefined,
                    menu: [
                        {
                            name: 'Crear usuarios',
                            icon: 'fa-address-card',
                            badge: 0,
                            menu: undefined
                        },
                        {
                            name: 'Modificar usuarios',
                            icon: 'fa-address-card',
                            badge: 0,
                            menu: undefined
                        },
                        {
                            name: 'Usuarios borrados',
                            icon: 'fa-address-card',
                            badge: 0,
                            menu: undefined
                        }
                    ]
                },
                {
                    name: 'Chat',
                    icon: 'fa-envelope',
                    badge: 3,
                    menu: [
                        {
                            name: 'Mensajes',
                            icon: 'fa-inbox',
                            badge: 3,
                            menu: undefined
                        },
                        {
                            name: 'Amigos en línea',
                            icon: 'fa-comment ',
                            badge: 9,
                            menu: undefined
                        },
                        {
                            name: 'Comunicados',
                            icon: 'fa-globe',
                            badge: 0,
                            menu: undefined
                        }
                    ]
                },
                {
                    name: 'Mi cuenta',
                    icon: 'fa-cogs',
                    badge: 3,
                    menu: [
                        {
                            name: 'Configuración',
                            icon: 'fa-wrench',
                            badge: 0,
                            menu: [
                            {
                                name : 'Contraseña',
                                icon: '',
                                badge: undefined,
                                menu: undefined
                            },
                            {
                                name : 'Correo',
                                icon: '',
                                badge: undefined,
                                menu: undefined
                            },
                            {
                                name : 'Eliminar cuenta',
                                icon: '',
                                badge: undefined,
                                menu: undefined
                            },
                            ]
                        },
                        {
                            name: 'Mis datos personales',
                            icon: 'sliders-h',
                            badge: 0,
                            menu: undefined
                        },
                        {
                            name: 'Cerrar sesión',
                            icon: 'fa-sign-out-alt',
                            badge: 0,
                            menu: undefined
                        }
                    ]
                }
            ]
        }
    )
    //.send(null); 
}