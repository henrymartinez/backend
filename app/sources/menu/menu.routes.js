const express = require('express');

const api = express.Router();

const controller = require('./menu.controller');


/*
api.get('/', function(req, res) {
    res.json({ mensaje: '¡Hola Mundo heriberto!' })   
})*/

api.get('/', controller.getMenu);
module.exports = api;