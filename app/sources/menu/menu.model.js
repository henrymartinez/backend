const Sequelize = require('sequelize');
const schema = require('../../sequelize')


module.exports = schema.define('Menu',{
    name: {type: Sequelize.STRING, allowNull: false},
    icon: {type: Sequelize.STRING, allowNull: false},
    badge: {type: Sequelize.STRING, allowNull: false}
}, {
    timestamps: false,
    freezeTableName: true
})