const express = require('express');

const api = express.Router();

const controller = require('./user.controller');

api.get('/', function(req, res, next) {
    res.send('respond with a resource');
  });
api.get('/login', controller.login);
api.delete('/logout', controller.logout );
api.post('/register', controller.register);

api.get('/list', controller.getList);

module.exports = api;