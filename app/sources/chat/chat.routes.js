const express = require('express');

const api = express.Router();

const controller = require('./chat.controller');


api.post('/write', controller.write);
api.get('/list', controller.getList);
api.get('/item', controller.getItem);
module.exports = api;